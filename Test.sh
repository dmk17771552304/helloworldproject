#!/bin/bash
##################
# 钉钉通知
# 自动化部署脚本中使用
##################

#!/bin/bash
 
webhook='https://oapi.dingtalk.com/robot/send?access_token=bb4960a0582699378e6e6cd9b14dedd8b04d6f702dbaed4fb69f2d03c69fe2e5'
 
function SendMsgToDingding() {
  curl $webhook -H 'Content-Type: application/json' -d "
  {
    'msgtype': 'text',
    'text': {
        'content': '[CI/CD]
Branch: ${CI_COMMIT_REF_NAME}
User: ${GITLAB_USER_EMAIL}
[Pipeline #${CI_PIPELINE_ID}]'
    },
    'at': {
		"atMobiles": [
		"17771552304",
		],
		'isAtAll': false
    }
  }"
}
SendMsgToDingding